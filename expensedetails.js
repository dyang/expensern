import React from 'react';
import ImagePicker from 'react-native-image-picker';
import { StyleSheet, View, Text, Button, FlatList, NativeModules } from 'react-native';
import { Separator, Cell } from 'react-native-tableview-simple';
import { TextInput } from 'react-native-gesture-handler';
import { name, formatDate } from './common.js'

function submitComment(id, text) {
    fetch('http://localhost:3000/expenses/' + id, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ comment: text })
    })
        .catch((error) => {
            console.error(error);
        });
}

class ExpenseDetailView extends React.Component {
    render() {
        const expense = this.props.expense;

        return (
            <View style={styles.detailContainer}>
                <View style={styles.detailItem}>
                    <Text style={styles.keyLabel}>User</Text>
                    <Text style={styles.valueLabel}>{name(expense.user)}</Text>
                </View>
                <View style={styles.detailItem}>
                    <Text style={styles.keyLabel}>Merchant</Text>
                    <Text style={styles.valueLabel}>{expense.merchant}</Text>
                </View>
                <View style={styles.detailItem}>
                    <Text style={styles.keyLabel}>Date</Text>
                    <Text style={styles.valueLabel}>{formatDate(expense.date)}</Text>
                </View>
                <View style={styles.detailItem}>
                    <Text style={styles.keyLabel}>Comment</Text>
                    <TextInput style={styles.comment}
                        clearButtonMode='while-editing'
                        placeholder='Leave a comment'
                        defaultValue={expense.comment}
                        onEndEditing={(e) => submitComment(expense.id, e.nativeEvent.text)} />
                </View>
            </View>
        );
    }
};

export default class ExpenseDetails extends React.Component {
    
    pickImage = () => {
        const options = {
            mediaType: 'photo'
        }
        ImagePicker.launchImageLibrary(options, (response) => {
            this.uploadImage(this.state.expense.id, response)
        })
    }

    uploadImage = (id, image) => {
        const url = 'http://localhost:3000/expenses/' + id + '/receipts';
        const callback = (json) => {
            if (json) {
                this.setState({
                    expense: json
                })
                alert('Uploaded!');
            }
        }
        NativeModules.FileUpload.upload(image.uri, url, callback);
    }

    showReceipt = (url) => {
        this.props.navigation.navigate('ReceiptDetails', { 'url': 'http://localhost:3000' + url });
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Expense',
            headerRight: (
                <Button
                    onPress={ navigation.getParam('pickImage') }
                    title="Add"
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = { expense: this.props.navigation.getParam('expense') };
    }

    componentDidMount() {
        this.props.navigation.setParams({ pickImage: this.pickImage });
    }

    render() {
        return (
            <View style={styles.container}>
                <ExpenseDetailView expense={this.state.expense} />
                <FlatList
                    data={this.state.expense.receipts}
                    keyExtractor={ item => item.url }
                    renderItem={({ item }) => (
                        <Cell
                            cellStyle='Basic'
                            title={item.url}
                            onPress={() => this.showReceipt(item.url)}
                        />
                    )}
                    ItemSeparatorComponent={({ highlighted }) => (
                        <Separator isHidden={highlighted} />
                    )}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    detailContainer: {
        height: 120,
        padding: 10,
    },
    detailItem: {
        height: 30,
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
    },
    keyLabel: {
        fontSize: 15,
        width: '40%',
    },
    valueLabel: {
        fontSize: 18,
        width: '60%',
        textAlign: 'right',
    },
    comment: {
        fontSize: 18,
        width: '60%',
        textAlign: 'right',
    }
})
