//
//  FileUpload.m
//  ExpenseRN
//
//  Created by Derek Yang on 2019-10-09.
//  Copyright © 2019 Derek Yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"

@interface RCT_EXTERN_MODULE(FileUpload, NSObject)

RCT_EXTERN_METHOD(upload:(NSURL *)file url:(NSString *)url callback:(RCTResponseSenderBlock)callback)

@end
