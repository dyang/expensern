//
//  FileUpload.swift
//  ExpenseRN
//
//  Created by Derek Yang on 2019-10-09.
//  Copyright © 2019 Derek Yang. All rights reserved.
//

import Foundation
import Alamofire

@objc(FileUpload)
class FileUpload: NSObject {
  
  @objc
  func upload(_ file: URL, url: String, callback: @escaping RCTResponseSenderBlock) {
    AF.upload(multipartFormData: { multiFormData in
      multiFormData.append(file, withName: "receipt")
    }, to: url).responseJSON { response in
      if let data = response.data, let json = try? JSONSerialization.jsonObject(with: data, options: []) {
        callback([json])
      } else {
        callback([])
      }
    }
  }
  
  @objc
  static func requiresMainQueueSetup() -> Bool {
    return false
  }
}
