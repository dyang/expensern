/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import ExpenseList from './expenselist';
import ExpenseDetails from './expensedetails';
import ReceiptDetails from './receiptdetails'

const Navigator = createStackNavigator({
  ExpenseList: { screen: ExpenseList },
  ExpenseDetails: { screen: ExpenseDetails },
  ReceiptDetails: { screen: ReceiptDetails }
})

const App = createAppContainer(Navigator);
export default App;
