import React from 'react';
import { FlatList, StyleSheet, View, ActivityIndicator } from 'react-native';
import { Cell, Separator } from 'react-native-tableview-simple';
import { name } from './common.js'

function fetchExpenses(completed) {
    return fetch('http://localhost:3000/expenses/')
            .then((response) => response.json())
            .then((responseJson) => {
                completed(responseJson);
            })
            .catch((error) => {
                console.error(error);
            });
}

export default class ExpenseList extends React.Component {
    static navigationOptions = {
        title: 'Expenses'
    };
    constructor(props) {
        super(props);
        this.state = { isLoading: true }
    }

    componentDidMount() {
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            fetchExpenses((json) => {
                this.setState({
                    isLoading: false,
                    dataSource: json.expenses,
                });
            });
        });
        return fetchExpenses((json) => {
            this.setState({
                isLoading: false,
                dataSource: json.expenses,
            });
        });
    }

    componentWillUnmount() {
        this.focusListener.remove();
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    <ActivityIndicator />
                </View>
            )
        }

        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.dataSource}
                    keyExtractor={ item => item.id}
                    renderItem={({ item }) => (
                        <Cell
                            cellStyle='RightDetail'
                            title={name(item.user)}
                            detail={item.amount.value + ' ' + item.amount.currency}
                            onPress={() => this.props.navigation.navigate('ExpenseDetails', { 'expense': item })}
                            accessory='DisclosureIndicator' />
                    )}
                    ItemSeparatorComponent={({ highlighted }) => (
                        <Separator isHidden={highlighted} />
                    )}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
})
