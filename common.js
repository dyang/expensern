import moment from 'moment';

export function name(user) {
    return user.first + ' ' + user.last;
}

export function formatDate(date) {
    return moment(date).format('YYYY/M/D');
}