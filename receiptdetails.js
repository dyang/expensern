import React from 'react';
import { StyleSheet, Image } from 'react-native';

export default class ReceiptDetails extends React.Component {
    static navigationOptions = {
        title: 'Receipt'
    }
    render() {
        return <Image source={{uri: this.props.navigation.getParam('url')}} style={styles.receiptImage} />
    }
}

const styles = StyleSheet.create({
    receiptImage: {
        flex: 1,
        resizeMode: 'contain'
    },
})